FROM golang:1.12
ENV GOPATH=/go
WORKDIR /go
RUN mkdir -p src/bitbucket.org/alexmourapb/pagar-me-api
COPY . /go/src/bitbucket.org/alexmourapb/pagar-me-api
WORKDIR /go/src/bitbucket.org/alexmourapb/pagar-me-api
RUN make build
FROM debian:buster-slim
RUN apt-get update && apt-get install ca-certificates -y && rm -rf /var/lib/apt/lists/*
WORKDIR /root/
COPY --from=0 /go/src/bitbucket.org/alexmourapb/pagar-me-api/pagar-me-api .
CMD ["./pagar-me-api"]