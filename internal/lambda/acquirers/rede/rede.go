package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/lambda/acquirers/process"
)

var redeLimit float64 = 60.0

func HandleCardAuthorizerRede(request events.APIGatewayProxyRequest) (response process.APIGatewayProxyResponse, err error) {

	return process.ProcessRequest(redeLimit, request)
}

func main() {

	lambda.Start(HandleCardAuthorizerRede)
}
