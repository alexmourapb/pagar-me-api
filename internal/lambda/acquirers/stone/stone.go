package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/lambda/acquirers/process"
)

var stoneLimit float64 = 100.0

func HandleCardAuthorizerStone(request events.APIGatewayProxyRequest) (response process.APIGatewayProxyResponse, err error) {

	return process.ProcessRequest(stoneLimit, request)
}

func main() {

	lambda.Start(HandleCardAuthorizerStone)
}
