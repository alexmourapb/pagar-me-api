package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/lambda/acquirers/process"
)

var cieloLimit float64 = 50.0

func HandleCardAuthorizerCielo(request events.APIGatewayProxyRequest) (response process.APIGatewayProxyResponse, err error) {

	return process.ProcessRequest(cieloLimit, request)
}

func main() {

	lambda.Start(HandleCardAuthorizerCielo)
}
