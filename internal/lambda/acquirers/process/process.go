package process

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/io/acquirer"
)

const (
	FixedClientID = "clientIDPagarmeTest"
	FixedPassword = "PagarmePasswordTest123*#"
)

type APIGatewayProxyResponse struct {
	StatusCode        int                 `json:"statusCode"`
	Headers           map[string]string   `json:"headers,omitempty"`
	MultiValueHeaders map[string][]string `json:"multiValueHeaders,omitempty"`
	Body              string              `json:"body"`
	IsBase64Encoded   bool                `json:"isBase64Encoded,omitempty"`
}

func validateSecret(request events.APIGatewayProxyRequest) error {

	clientID := request.Headers["client-id"]
	clientSecret := request.Headers["client-secret"]

	if clientID != FixedClientID || clientSecret != FixedPassword {
		return errors.New("invalid credentials")
	}

	return nil
}

func ProcessRequest(limit float64, request events.APIGatewayProxyRequest) (APIGatewayProxyResponse, error) {

	response := &APIGatewayProxyResponse{}
	responseError := &APIGatewayProxyResponse{
		StatusCode: http.StatusBadRequest,
		Body:       http.StatusText(http.StatusForbidden),
	}

	err := validateSecret(request)
	if err != nil {
		return *responseError, err
	}

	body := []byte(request.Body)

	var getAuthorizationRequest acquirer.AcquirerAuthorizationRequest
	err = json.Unmarshal(body, &getAuthorizationRequest)
	if err != nil {
		return *responseError, err
	}

	var bodyResponseMap acquirer.AcquirerAuthorizationResponse

	limitAmount := decimal.NewFromFloat(limit)

	amount, _ := decimal.NewFromString(getAuthorizationRequest.Amount)

	bodyResponseMap.AuthID = uuid.NewV4().String()
	if amount.GreaterThan(limitAmount) {
		bodyResponseMap.Status = "denied"
		bodyResponseMap.Message = "insufficient funds"
		bodyResponse, _ := json.Marshal(bodyResponseMap)
		response.StatusCode = http.StatusOK
		response.Body = string(bodyResponse)
		return *response, nil
	}

	bodyResponseMap.Status = "approved"
	bodyResponseMap.Message = "approved ok"
	bodyResponse, _ := json.Marshal(bodyResponseMap)
	response.StatusCode = http.StatusOK
	response.Body = string(bodyResponse)

	return *response, nil
}
