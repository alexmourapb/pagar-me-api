package process

import (
	"testing"

	"github.com/aws/aws-lambda-go/events"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/test"
)

func Test_ProcessRequest_Success(t *testing.T) {

	body := `{"amount": "10"}`

	headers := map[string]string{}
	headers["Cache-Control"] = "no-cache"
	headers["client-id"] = FixedClientID
	headers["client-secret"] = FixedPassword
	headers["Content-Type"] = "application/json"

	request := events.APIGatewayProxyRequest{
		Headers: headers,
		Body:    body,
	}

	limit := 20.0

	_, err := ProcessRequest(limit, request)
	test.AssertOk(t, err)
}

func Test_ProcessRequest_AmountGreaterThanLimit(t *testing.T) {

	body := `{"amount": "50"}`

	headers := map[string]string{}
	headers["Cache-Control"] = "no-cache"
	headers["client-id"] = FixedClientID
	headers["client-secret"] = FixedPassword
	headers["Content-Type"] = "application/json"

	request := events.APIGatewayProxyRequest{
		Headers: headers,
		Body:    body,
	}

	limit := 10.0

	_, err := ProcessRequest(limit, request)
	test.AssertOk(t, err)
}

func Test_ProcessRequest_WithInvalidPassword(t *testing.T) {

	body := `{"amount": "50"}`

	headers := map[string]string{}
	headers["Cache-Control"] = "no-cache"
	headers["client-id"] = FixedClientID
	headers["client-secret"] = ""
	headers["Content-Type"] = "application/json"

	request := events.APIGatewayProxyRequest{
		Headers: headers,
		Body:    body,
	}

	limit := 10.0

	_, err := ProcessRequest(limit, request)
	test.AssertNotOk(t, err)
}
