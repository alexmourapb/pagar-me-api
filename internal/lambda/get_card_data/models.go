package main

import (
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/endpoints"
)

const (
	region = endpoints.UsEast1RegionID

	StatusActive = "active"

	secretTableName       = "auth-manager_secret"
	customerDataTableName = "customer_data"
)

var (
	awsAccessKey          string
	awsAccessSecret       string
	awsSecretsManagerName string
	awsCredentials        *credentials.Credentials
)

func dynamoCustomerDataTableName() string {
	return customerDataTableName
}

type Secret struct {
	ID       string `dynamo:"id" json:"client-id"`
	Secret   string `dynamo:"secret" json:"client-secret"`
	IssuerID string `dynamo:"issuer-id" json:"issuer-id"`
	Status   string `dynamo:"status" json:"status" valid:"in(active|blocked)"`
}

func DynamoSecretTableName() string {
	return secretTableName
}

type APIGatewayProxyResponse struct {
	StatusCode        int                 `json:"statusCode"`
	Headers           map[string]string   `json:"headers,omitempty"`
	MultiValueHeaders map[string][]string `json:"multiValueHeaders,omitempty"`
	Body              string              `json:"body"`
	IsBase64Encoded   bool                `json:"isBase64Encoded,omitempty"`
}

type LambdaGetCardNumberRequest struct {
	CustomerID string `json:"customerID"`
}

type LambdaGetCardNumberResponse struct {
	CardNumber string `json:"cardNumber"`
	CVV        string `json:"cvv"`
}
