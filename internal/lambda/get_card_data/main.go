package main

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/guregu/dynamo"
)

//Get encryption key from AWS Secrets Manager service
func getSecret(secretName string) (*secretsmanager.GetSecretValueOutput, error) {

	sess := session.Must(session.NewSession(&aws.Config{
		Region:      aws.String(region),
		Credentials: awsCredentials,
	}))

	svc := secretsmanager.New(sess)
	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secretName),
	}

	result, err := svc.GetSecretValue(input)
	if err != nil {
		return nil, err
	}

	return result, nil
}

//Decrypts the data
func decrypt(key []byte, secureMess string) (string, error) {

	var decodedMess string

	cipherText, err := base64.URLEncoding.DecodeString(secureMess)
	if err != nil {
		return decodedMess, err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return decodedMess, err
	}

	if len(cipherText) < aes.BlockSize {
		err = errors.New("cipher text block size is too short")
		return decodedMess, err
	}

	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(cipherText, cipherText)

	decodedMess = string(cipherText)
	return decodedMess, nil
}

//Get encrypted card data from Dynamodb
func getDynamoCustomerData(customerID string) (*string, error) {

	var svc *dynamo.DB
	svc = dynamo.New(session.New(), &aws.Config{
		Credentials: awsCredentials,
		Region:      aws.String(region)})

	var dynamoResponse map[string]interface{}
	err := svc.Table(dynamoCustomerDataTableName()).Get("id", customerID).One(&dynamoResponse)
	if err != nil {
		return nil, err
	}

	cardData, _ := dynamoResponse["cardData"].(string)

	return &cardData, nil
}

//Validates Client-Id and Client-Secret data sent in header
func validateSecret(clientID, clientSecret string) bool {

	svc := dynamo.New(session.New(), &aws.Config{
		Credentials: credentials.NewStaticCredentials(awsAccessKey, awsAccessSecret, ""),
		Region:      aws.String("us-east-1")})

	var secret Secret
	err := svc.Table(DynamoSecretTableName()).Get("id", clientID).One(&secret)
	if err != nil {
		return false
	}

	if secret.Secret != clientSecret || secret.Status != StatusActive {
		return false
	}

	return true
}

//Check environment variables
func checkEnvironmentVariables() error {

	if awsAccessKey == "" {
		return errors.New("invalid aws access key")
	}

	if awsAccessSecret == "" {
		return errors.New("invalid aws access secret")
	}

	if awsSecretsManagerName == "" {
		return errors.New("invalid aws secrets manager name")
	}

	return nil
}

//Handler responsible for retrieving and returning a customer's card data
func HandleLambdaGetCardNumber(request events.APIGatewayProxyRequest) (APIGatewayProxyResponse, error) {

	response := &APIGatewayProxyResponse{}
	responseError := &APIGatewayProxyResponse{
		StatusCode: http.StatusBadRequest,
		Body:       http.StatusText(http.StatusForbidden),
	}

	clientID := request.Headers["client-id"]
	clientSecret := request.Headers["client-secret"]

	if !validateSecret(clientID, clientSecret) {
		return *responseError, errors.New("invalid credentials")
	}

	body := []byte(request.Body)

	var getCardNumberRequest LambdaGetCardNumberRequest
	err := json.Unmarshal(body, &getCardNumberRequest)
	if err != nil {
		return *responseError, err
	}

	awsSecret, err := getSecret(awsSecretsManagerName)
	if err != nil {
		return *responseError, err
	}

	b := []byte(*awsSecret.SecretString)

	type secretKey struct {
		CypherKey string `json:"cypherKey"`
	}

	var secret secretKey
	err = json.Unmarshal(b, &secret)
	if err != nil {
		return *responseError, err
	}

	secretDecoded, err := base64.StdEncoding.DecodeString(secret.CypherKey)
	if err != nil {
		return *responseError, err
	}

	cardData64, err := getDynamoCustomerData(getCardNumberRequest.CustomerID)
	if err != nil {
		return *responseError, err
	}

	cardDataEncrypted, err := base64.StdEncoding.DecodeString(*cardData64)
	if err != nil {
		return *responseError, err
	}
	cardDataEncryptedString := string(cardDataEncrypted)
	cardDataDecrypted, err := decrypt(secretDecoded, cardDataEncryptedString)
	if err != nil {
		return *responseError, err
	}

	bodyResponseMap := &LambdaGetCardNumberResponse{
		CardNumber: cardDataDecrypted[:16],
		CVV:        cardDataDecrypted[16:],
	}

	bodyResponse, _ := json.Marshal(bodyResponseMap)

	response.StatusCode = http.StatusOK
	response.Body = string(bodyResponse)

	return *response, nil
}

func init() {
	awsAccessKey = os.Getenv("PAGARME_AWS_ACCESS")
	awsAccessSecret = os.Getenv("PAGARME_AWS_SECRET")
	awsSecretsManagerName = os.Getenv("PAGARME_AWS_SECRET_MANAGER_NAME")
	awsCredentials = credentials.NewStaticCredentials(awsAccessKey, awsAccessSecret, "")
}

func main() {

	err := checkEnvironmentVariables()
	if err != nil {
		return
	}

	lambda.Start(HandleLambdaGetCardNumber)
}
