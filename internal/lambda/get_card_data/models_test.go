package main

import (
	"errors"
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Get_Card_Structs(t *testing.T) {

	type Fields struct {
		fieldName string
		fieldType string
		tags      string
	}

	var tt = []struct {
		modelName   string
		modelStruct interface{}
		fields      []Fields
	}{
		//Test Secret struct
		{"Secret", &Secret{}, []Fields{
			{"ID", "string", `dynamo:"id" json:"client-id"`},
			{"Secret", "string", `dynamo:"secret" json:"client-secret"`},
			{"IssuerID", "string", `dynamo:"issuer-id" json:"issuer-id"`},
			{"Status", "string", `dynamo:"status" json:"status" valid:"in(active|blocked)"`}}},

		//Test APIGatewayProxyResponse struct
		{"APIGatewayProxyResponse", &APIGatewayProxyResponse{}, []Fields{
			{"StatusCode", "int", `json:"statusCode"`},
			{"Headers", "map[string]string", `json:"headers,omitempty"`},
			{"MultiValueHeaders", "map[string][]string", `json:"multiValueHeaders,omitempty"`},
			{"Body", "string", `json:"body"`},
			{"IsBase64Encoded", "bool", `json:"isBase64Encoded,omitempty"`}}},

		//Test LambdaGetCardNumberRequest struct
		{"LambdaGetCardNumberRequest", &LambdaGetCardNumberRequest{}, []Fields{
			{"CustomerID", "string", `json:"customerID"`}}},

		//Test LambdaGetCardNumberResponse struct
		{"LambdaGetCardNumberResponse", &LambdaGetCardNumberResponse{}, []Fields{
			{"CardNumber", "string", `json:"cardNumber"`},
			{"CVV", "string", `json:"cvv"`}}},
	}
	for _, tc := range tt {

		reflectionElements := reflect.ValueOf(tc.modelStruct).Elem()
		for i, field := range tc.fields {

			testName := fmt.Sprintf("%s %s", tc.modelName, field.fieldName)
			t.Run(testName, func(t *testing.T) {

				// Check field name
				v := reflectionElements.FieldByName(field.fieldName)
				assert.True(t, v.IsValid(), "Field '%s' not found in the struct!", field.fieldName)

				// Check field type
				ts := fmt.Sprintf("%v", v.Type())
				assert.Equal(t, ts, field.fieldType,
					"Field '%s' should be of type '%s', but received '%s'",
					field.fieldName, field.fieldType, ts)

				// Check field tag
				tag := fmt.Sprintf("%s", reflectionElements.Type().Field(i).Tag)
				assert.Equal(t, tag, field.tags,
					"Field '%s' tag changed. Tag expected: %s",
					field.fieldName, tag)
			})

		}

		assert.Equal(t, len(tc.fields), reflectionElements.NumField(),
			"Found %d elements in struct (%s) while waiting for %d",
			len(tt), tc.modelName, reflectionElements.NumField())
	}
}

func Test_DynamoSecretTableName(t *testing.T) {

	if DynamoSecretTableName() != secretTableName {
		t.Fatal(errors.New("DynamoSecretTableName is invalid"))
	}
}

func Test_dynamoCustomerDataTableName(t *testing.T) {

	if dynamoCustomerDataTableName() != customerDataTableName {
		t.Fatal(errors.New("dynamoCustomerDataTableName is invalid"))
	}
}
