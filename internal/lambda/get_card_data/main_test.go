package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"io"
	"testing"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/test"
)

func Test_checkEnvironmentVariables(t *testing.T) {

	err := checkEnvironmentVariables()
	test.AssertNotOk(t, err)

	awsAccessKey = "testeAccesskey"
	err = checkEnvironmentVariables()
	test.AssertNotOk(t, err)

	awsAccessSecret = "testeSecret"
	err = checkEnvironmentVariables()
	test.AssertNotOk(t, err)

	awsSecretsManagerName = "managerName"
	err = checkEnvironmentVariables()
	test.AssertOk(t, err)
}

func Test_decrypt(t *testing.T) {

	key := "testKey"
	text := "testText"
	plainKey := []byte(key)
	plainText := []byte(text)

	block, err := aes.NewCipher(plainKey)
	if err != nil {
		return
	}

	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], plainText)

	encmess := base64.URLEncoding.EncodeToString(cipherText)

	decrypted, err := decrypt(plainKey, encmess)

	if decrypted != text {
		t.Fatal(errors.New("decrypt invalid"))
	}
}
