package middleware

import (
	"net/http"

	"github.com/rs/zerolog"
)

//RouteMonitoring ...
func RouteMonitoring(method string, route string, logger *zerolog.Logger, fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		monitoringMsg := " PATH=" + method + "_" + route + ""
		if logger != nil {
			logger.Info().Msg(monitoringMsg)
		}

		fn(w, r)
	}
}
