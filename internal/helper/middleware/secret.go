package middleware

import (
	"context"
	"net/http"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

const (
	StatusActive = "active"

	clientIDWidth     = 22
	clientSecretWidth = 33

	ClientIDHeaderName     = "Client-Id"
	ClientSecretHeaderName = "Client-Secret"
	IssuerIDContextName    = "issuer-id"
)

func DynamoSecretTableName() string {
	return "auth-manager_secret"
}

type Secret struct {
	ID       string `dynamo:"id" json:"client-id"`
	Secret   string `dynamo:"secret" json:"client-secret"`
	IssuerID string `dynamo:"issuer-id" json:"issuer-id"`
	Status   string `dynamo:"status" json:"status" valid:"in(active|blocked)"`
}

//Checks whether the data sent confer with stored data in DynamoDB
func ValidateSecret(r *http.Request, clientID, clientSecret string) (bool, string) {

	var sourceID string

	if len(clientID) < clientIDWidth || len(clientSecret) < clientSecretWidth {
		return false, sourceID
	}

	awsAccessKey := os.Getenv("PAGARME_AWS_ACCESS")
	if awsAccessKey == "" {
		return false, sourceID
	}

	awsSecret := os.Getenv("PAGARME_AWS_SECRET")
	if awsSecret == "" {
		return false, sourceID
	}

	svc := dynamo.New(session.New(), &aws.Config{
		Credentials: credentials.NewStaticCredentials(awsAccessKey, awsSecret, ""),
		Region:      aws.String("us-east-1")})

	var secret Secret
	err := svc.Table(DynamoSecretTableName()).Get("id", clientID).One(&secret)
	if err != nil {
		return false, sourceID
	}

	if secret.Secret != clientSecret || secret.Status != StatusActive {
		return false, sourceID
	}

	return true, secret.IssuerID
}

//This middleware validates the client-id and client-secret sent in the request header.
func SecretMiddleware() func(next http.Handler) http.Handler {

	return func(next http.Handler) http.Handler {

		fn := func(w http.ResponseWriter, r *http.Request) {

			clientID := r.Header.Get(ClientIDHeaderName)
			clientSecret := r.Header.Get(ClientSecretHeaderName)

			if strings.TrimSpace(clientID) == "" || strings.TrimSpace(clientSecret) == "" {
				http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
				return
			}

			valid, sourceID := ValidateSecret(r, clientID, clientSecret)
			if !valid {
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}

			ctx := context.WithValue(r.Context(), IssuerIDContextName, sourceID)
			r = r.WithContext(ctx)

			next.ServeHTTP(w, r)
		}

		return http.HandlerFunc(fn)
	}
}
