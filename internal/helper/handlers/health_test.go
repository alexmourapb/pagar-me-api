package handlers_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	apiHandlers "bitbucket.org/alexmourapb/pagar-me-api/cmd/api/handlers"
	apiRouters "bitbucket.org/alexmourapb/pagar-me-api/cmd/api/routers"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/infrastructure/environment"
	apiLogger "bitbucket.org/alexmourapb/pagar-me-api/internal/helper/infrastructure/logger"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/test"
)

func TestHealthCheckHandle(t *testing.T) {

	r, err := http.NewRequest("GET", "/health", nil)
	if err != nil {
		t.Error(err.Error())
	}

	logger := apiLogger.Config("pagar-me-test")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{DB: nil, Logger: &logger, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusOK, w.Code)
}
