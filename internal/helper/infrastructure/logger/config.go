package logger

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
)

func Config(serviceName string) zerolog.Logger {

	logger := zerolog.New(os.Stdout).With().Timestamp().Logger()

	logger.Info().Msg(fmt.Sprintf("%s Service", serviceName))

	return logger
}
