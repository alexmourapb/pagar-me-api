package postgres

import (
	"log"
	"os"

	"github.com/jinzhu/gorm"
)

//NewDBTest ...
func NewDBTest() *gorm.DB {
	dbURI := os.Getenv("POSTGRESDBTEST_URI")
	if dbURI == "" {
		dbURI = "postgres://postgres:postgres@localhost:5432/testdb?sslmode=disable"
	}

	db, err := gorm.Open("postgres", dbURI)
	if err != nil {
		log.Println("Error DB: " + err.Error() + " " + dbURI)
		os.Exit(1)
	}
	return db
}
