package authorization

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/customer"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/issuer"
)

const (
	authorizationTableName                = "authorizations"
	authorizationTransactionItemTableName = "authorization_transaction_items"
)

type Authorization struct {
	ID uuid.UUID `sql:",pk,type:uuid" json:"id"`

	Currency             string          `sql:"default:'BRL'" json:"currency"`
	Amount               decimal.Decimal `sql:"type:numeric" json:"amount"`
	NumberOfInstallments uint            `sql:",notnull" json:"number-of-installments"`

	// Customer Account
	CustomerAccount   *customer.CustomerAccount `json:"-"`
	CustomerAccountID uuid.UUID                 `sql:",type:uuid" json:"-"`

	// Issuer
	Issuer   *issuer.Issuer `json:"-"`
	IssuerID uuid.UUID      `sql:",notnull,type:uuid" json:"issuer-id"`

	// Transaction
	TransactionID uuid.UUID `sql:",notnull,type:uuid" json:"transaction-id"`

	Status string `sql:"default:'pending'" json:"status"`

	// Acquirer Response
	AcquirerAuthID          string `json:"acquirer-auth-id"`
	AcquirerStatusResponse  string `json:"acquirer-status-response"`
	AcquirerMessageResponse string `json:"acquirer-message-response"`

	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}

func (Authorization) TableName() string {
	return authorizationTableName
}

func (a *Authorization) BeforeInsert() {

	a.ID = uuid.NewV4()
	a.CreatedAt = time.Now()

	return
}

func (a *Authorization) BeforeUpodate() {

	a.UpdatedAt = time.Now()

	return
}

type AuthorizationTransactionItem struct {
	ID              uuid.UUID `sql:",pk,type:uuid" json:"-"`
	TransactionID   uuid.UUID `sql:",notnull,type:uuid" json:"-"`
	AuthorizationID uuid.UUID `sql:",notnull,type:uuid" json:"-"`

	CodItem     string          `sql:",notnull" json:"cod-item"`
	Descriprion string          `sql:",notnull" json:"description"`
	Amount      decimal.Decimal `sql:"type:numeric" json:"amount"`
}

func (a *AuthorizationTransactionItem) BeforeInsert() {

	a.ID = uuid.NewV4()

	return
}

func (AuthorizationTransactionItem) TableName() string {
	return authorizationTransactionItemTableName
}
