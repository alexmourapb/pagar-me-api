package authorization

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Authorization_Structs(t *testing.T) {

	type Fields struct {
		fieldName string
		fieldType string
		tags      string
	}

	var tt = []struct {
		modelName   string
		modelStruct interface{}
		fields      []Fields
	}{
		//Test Authorization struct
		{"Authorization", &Authorization{}, []Fields{
			{"ID", "uuid.UUID", `sql:",pk,type:uuid" json:"id"`},
			{"Currency", "string", `sql:"default:'BRL'" json:"currency"`},
			{"Amount", "decimal.Decimal", `sql:"type:numeric" json:"amount"`},
			{"NumberOfInstallments", "uint", `sql:",notnull" json:"number-of-installments"`},
			{"CustomerAccount", "*customer.CustomerAccount", `json:"-"`},
			{"CustomerAccountID", "uuid.UUID", `sql:",type:uuid" json:"-"`},
			{"Issuer", "*issuer.Issuer", `json:"-"`},
			{"IssuerID", "uuid.UUID", `sql:",notnull,type:uuid" json:"issuer-id"`},
			{"TransactionID", "uuid.UUID", `sql:",notnull,type:uuid" json:"transaction-id"`},
			{"Status", "string", `sql:"default:'pending'" json:"status"`},
			{"AcquirerAuthID", "string", `json:"acquirer-auth-id"`},
			{"AcquirerStatusResponse", "string", `json:"acquirer-status-response"`},
			{"AcquirerMessageResponse", "string", `json:"acquirer-message-response"`},
			{"CreatedAt", "time.Time", `json:"-"`},
			{"UpdatedAt", "time.Time", `json:"-"`}}},
	}
	for _, tc := range tt {

		reflectionElements := reflect.ValueOf(tc.modelStruct).Elem()
		for i, field := range tc.fields {

			testName := fmt.Sprintf("%s %s", tc.modelName, field.fieldName)
			t.Run(testName, func(t *testing.T) {

				// Check field name
				v := reflectionElements.FieldByName(field.fieldName)
				assert.True(t, v.IsValid(), "Field '%s' not found in the struct!", field.fieldName)

				// Check field type
				ts := fmt.Sprintf("%v", v.Type())
				assert.Equal(t, ts, field.fieldType,
					"Field '%s' should be of type '%s', but received '%s'",
					field.fieldName, field.fieldType, ts)

				// Check field tag
				tag := fmt.Sprintf("%s", reflectionElements.Type().Field(i).Tag)
				assert.Equal(t, tag, field.tags,
					"Field '%s' tag changed. Tag expected: %s",
					field.fieldName, tag)
			})

		}

		assert.Equal(t, len(tc.fields), reflectionElements.NumField(),
			"Found %d elements in struct (%s) while waiting for %d",
			len(tt), tc.modelName, reflectionElements.NumField())
	}
}
