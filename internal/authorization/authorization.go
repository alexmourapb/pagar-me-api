package authorization

import (
	"github.com/jinzhu/gorm"
)

const StatusProcessed = "processed"

func InsertAuthorization(db *gorm.DB, input *Authorization) (*Authorization, error) {

	input.BeforeInsert()

	if err := Create(db, input); err != nil {
		return nil, err
	}

	return input, nil
}

func FinishAuthorization(db *gorm.DB, authorizationID, acquirerStatus, acquirerAuthID, acquirerMessage string) (*Authorization, error) {

	authorization, err := GetAuthorizationByID(db, authorizationID)
	if err != nil {
		return nil, err
	}

	authorization.Status = StatusProcessed
	authorization.AcquirerAuthID = acquirerAuthID
	authorization.AcquirerStatusResponse = acquirerStatus
	authorization.AcquirerMessageResponse = acquirerMessage

	if err := Update(db, authorization); err != nil {
		return nil, err
	}

	return authorization, nil
}

func InsertAuthorizationTransactionItem(db *gorm.DB, input *AuthorizationTransactionItem) (*AuthorizationTransactionItem, error) {

	input.BeforeInsert()

	if err := CreateAuthorizationTransactionItem(db, input); err != nil {
		return nil, err
	}

	return input, nil
}
