package authorization

import (
	"testing"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/test"
)

func Test_InsertAuthorization(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Authorization{}.TableName())

	db.AutoMigrate(Authorization{})

	amount := decimal.NewFromFloat(10.0)
	transactionID := uuid.NewV4()
	accountID := uuid.NewV4()
	issuerID := uuid.NewV4()

	authorizationInput := &Authorization{
		Amount:               amount,
		NumberOfInstallments: 4,
		CustomerAccountID:    accountID,
		IssuerID:             issuerID,
		TransactionID:        transactionID,
	}

	authorizationOutput, err := InsertAuthorization(db, authorizationInput)
	if err != nil {
		t.Fatal(err.Error())
	}

	if authorizationOutput.TransactionID != transactionID {
		t.Fatal("autorizationOutput.TransactionID != transactionID")
	}
}

func Test_FinishAuthorization(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Authorization{}.TableName())

	db.AutoMigrate(Authorization{})

	amount := decimal.NewFromFloat(10.0)
	transactionID := uuid.NewV4()
	accountID := uuid.NewV4()
	issuerID := uuid.NewV4()
	authID := uuid.NewV4()

	authorizationInput := &Authorization{
		Amount:               amount,
		NumberOfInstallments: 4,
		CustomerAccountID:    accountID,
		IssuerID:             issuerID,
		TransactionID:        transactionID,
	}

	authorizationOutput, err := InsertAuthorization(db, authorizationInput)
	if err != nil {
		t.Fatal(err.Error())
	}

	acquirerAuthID := authID.String()
	acquirerStatus := "approved"
	acquirerMessage := "approved ok"

	authorizationOutput, err = FinishAuthorization(db, authorizationOutput.ID.String(), acquirerStatus, acquirerAuthID, acquirerMessage)
	if err != nil {
		t.Fatal(err.Error())
	}

	if authorizationOutput.Status != StatusProcessed {
		t.Fatal("authorizationOutput.Status != StatusProcessed")
	}
}

func Test_InsertAuthorizationTransactionItem(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(AuthorizationTransactionItem{})

	amount := decimal.NewFromFloat(10.0)
	transactionID := uuid.NewV4()

	authorizationTransactionItemInput := &AuthorizationTransactionItem{
		Amount:        amount,
		TransactionID: transactionID,
		CodItem:       "CodItemTest",
		Descriprion:   "Description test",
	}

	authorizationTransactionItemOutput, err := InsertAuthorizationTransactionItem(db, authorizationTransactionItemInput)
	if err != nil {
		t.Fatal(err.Error())
	}

	if authorizationTransactionItemOutput.TransactionID != transactionID {
		t.Fatal("authorizationTransactionItemOutput.TransactionID != transactionID")
	}
}
