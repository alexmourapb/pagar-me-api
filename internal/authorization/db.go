package authorization

import (
	"time"

	"github.com/jinzhu/gorm"
)

func Create(db *gorm.DB, authorization *Authorization) error {

	if err := db.Create(authorization).Error; err != nil {
		return err
	}

	return nil
}

func Update(db *gorm.DB, authorization *Authorization) error {

	authorization.UpdatedAt = time.Now()

	if err := db.Save(authorization).Error; err != nil {
		return err
	}

	return nil
}

func GetAuthorizationByID(db *gorm.DB, id string) (*Authorization, error) {

	authorization := &Authorization{}

	if err := db.First(authorization, "id = ?", id).Error; err != nil {
		return nil, err
	}

	return authorization, nil
}

func CreateAuthorizationTransactionItem(db *gorm.DB, transactionItem *AuthorizationTransactionItem) error {

	if err := db.Create(transactionItem).Error; err != nil {
		return err
	}

	return nil
}
