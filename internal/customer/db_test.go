package customer

import (
	"testing"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "github.com/satori/go.uuid"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/test"
)

func Test_GetCustomerByCPF(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Customer{}.TableName())

	db.AutoMigrate(Customer{})

	customerID := uuid.NewV4()
	cpf := "02792449403"

	customer := &Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	db.Create(customer)

	customerOutput, err := GetCustomerByCPF(db, cpf)
	if err != nil {
		t.Fatal(err.Error())
	}

	if customerOutput.ID != customerID {
		t.Fatal("customerOutput.ID != customerID")
	}
}

func Test_GetCustomerAccountByID(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(CustomerAccount{}.TableName())

	db.AutoMigrate(CustomerAccount{})

	customerAccountID := uuid.NewV4()
	externalID := uuid.NewV4().String()

	customerAccount := &CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     uuid.NewV4(),
		ExternalID:     externalID,
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(customerAccount)

	customerAccountOutput, err := GetCustomerAccountByID(db, customerAccountID.String())
	if err != nil {
		t.Fatal(err.Error())
	}

	if customerAccountOutput.ExternalID != externalID {
		t.Fatal("customerAccountOutput.ExternalID != externalID")
	}
}
