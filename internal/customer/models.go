package customer

import (
	uuid "github.com/satori/go.uuid"
)

const (
	customerTableName        = "customers"
	customerAccountTableName = "customer_accounts"
)

type Customer struct {
	ID uuid.UUID `sql:",pk,type:uuid" json:"id"`

	CPF         string `sql:",notnull,unique" json:"cpf"`
	Name        string `json:"name"`
	PhoneNumber string `json:"phone-number"`

	Status string `json:"status"`
}

func (Customer) TableName() string {
	return customerTableName
}

type CustomerAccount struct {
	ID uuid.UUID `sql:",pk,type:uuid" json:"id"`

	Customer   *Customer `json:"-"`
	CustomerID uuid.UUID `sql:",type:uuid" json:"-"`

	ExternalID     string `json:"-"`
	ExpirationDate string `json:"-"`
	CardBrand      string `json:"-"`

	Status string `json:"status"`
}

func (CustomerAccount) TableName() string {
	return customerAccountTableName
}
