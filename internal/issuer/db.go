package issuer

import (
	"github.com/jinzhu/gorm"
)

func GetIssuerByID(db *gorm.DB, id string) (*Issuer, error) {

	issuer := &Issuer{}

	if err := db.First(issuer, "id = ?", id).Error; err != nil {
		return nil, err
	}

	return issuer, nil
}
