package issuer

import (
	"testing"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "github.com/satori/go.uuid"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/test"
)

func Test_GetIssuerByID(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Issuer{}.TableName())

	db.AutoMigrate(Issuer{})

	issuerID := uuid.NewV4()
	cnpj := "02559765000191"

	issuer := &Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   cnpj,
		Status: "active",
	}

	db.Create(issuer)

	issuerOutput, err := GetIssuerByID(db, issuerID.String())
	if err != nil {
		t.Fatal(err.Error())
	}

	if issuerOutput.CNPJ != cnpj {
		t.Fatal("issuerOutput.CNPJ != cnpj")
	}
}
