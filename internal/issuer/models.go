package issuer

import (
	uuid "github.com/satori/go.uuid"
)

const (
	issuerTableName = "issuers"
)

type Issuer struct {
	ID uuid.UUID `sql:",pk,type:uuid" json:"id"`

	CNPJ string `sql:",notnull,unique" json:"cnpj"`
	Name string `json:"name"`

	Status string `json:"status"`
}

func (Issuer) TableName() string {
	return issuerTableName
}
