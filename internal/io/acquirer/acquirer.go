package acquirer

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/io/get_card"
)

const (
	envClientID     = "ACQUIRER_CLIENTID"
	envClientSecret = "ACQUIRER_SECRET"

	applicationJSON = "application/json"
	method          = "POST"
	cacheControl    = "no-cache"

	stoneURL = "https://14gdgpmwrh.execute-api.us-east-1.amazonaws.com/hmg/authstone"
	cieloURL = "https://14gdgpmwrh.execute-api.us-east-1.amazonaws.com/hmg/authcielo"
	redeURL  = "https://14gdgpmwrh.execute-api.us-east-1.amazonaws.com/hmg/authrede"
)

type AcquirerAuthorizationRequest struct {
	AccountNumber        string `json:"accountNumber"`
	CardCvv2Value        string `json:"cvv2Value"`
	CardExpiryDate       string `json:"cardExpiryDate"`
	OrderID              string `json:"orderId"`
	Name                 string `json:"name,omitempty"`
	Amount               string `json:"amount"`
	Currency             string `json:"currency"`
	NumberOfInstallments string `json:"number-of-installments"`
}

type AcquirerAuthorizationResponse struct {
	AuthID  string `json:"auth-id"`
	Status  string `json:"status"`
	Message string `json:"message"`
}

func GetAuthorization(cardBrand string, request *AcquirerAuthorizationRequest) (*AcquirerAuthorizationResponse, error) {

	var response AcquirerAuthorizationResponse
	var url string

	switch strings.ToLower(cardBrand) {
	case "mastercard":
		url = stoneURL
	case "visa":
		url = stoneURL
	case "elo":
		url = cieloURL
	case "hipercard":
		url = redeURL
	default:
		return nil, errors.New("invalid acquirer")
	}

	body := &bytes.Buffer{}
	encoder := json.NewEncoder(body)
	if err := encoder.Encode(&request); err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	clientID := os.Getenv(envClientID)
	clientSecret := os.Getenv(envClientSecret)
	if clientID == "" || clientSecret == "" {
		return nil, errors.New("failed to get acquirer credentials")
	}

	req.Header.Add("Cache-Control", cacheControl)
	req.Header.Add("client-id", clientID)
	req.Header.Add("client-secret", clientSecret)
	req.Header.Add("Content-Type", applicationJSON)

	// Send the HTTP request
	client := new(http.Client)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("failed to get acquirer authorization")
	}

	defer resp.Body.Close()

	reader, err := get_card.GetBody(resp.Body, resp.Header)
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(reader)
	if err := decoder.Decode(&response); err != nil {
		return nil, err
	}

	return &response, nil
}
