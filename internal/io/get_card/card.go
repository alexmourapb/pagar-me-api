package get_card

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"os"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/middleware"
)

const (
	contentEnconding = "Content-Encoding"
	applicationJSON  = "application/json"
	method           = "POST"
	cacheControl     = "no-cache"
)

type getCardNumberRequest struct {
	CustomerID string `json:"customerID"`
}

type getCardNumberResponse struct {
	CardNumber string `json:"cardNumber"`
	CVV        string `json:"cvv"`
}

func GetCardData(r *http.Request, externalID string) (*getCardNumberResponse, error) {

	var response getCardNumberResponse

	request := getCardNumberRequest{
		CustomerID: externalID,
	}

	body := &bytes.Buffer{}
	encoder := json.NewEncoder(body)
	if err := encoder.Encode(&request); err != nil {
		return nil, err
	}

	url := os.Getenv("HSM_URL")

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Cache-Control", cacheControl)
	req.Header.Add("Client-Id", r.Header.Get(middleware.ClientIDHeaderName))
	req.Header.Add("Client-Secret", r.Header.Get(middleware.ClientSecretHeaderName))
	req.Header.Add("Content-Type", applicationJSON)

	// Send the HTTP request
	client := new(http.Client)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("failed to get card information")
	}

	defer resp.Body.Close()

	reader, err := GetBody(resp.Body, resp.Header)
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(reader)
	if err := decoder.Decode(&response); err != nil {
		return nil, err
	}

	return &response, nil
}

func GetBody(b io.ReadCloser, h http.Header) (io.ReadCloser, error) {
	var reader io.ReadCloser
	var err error

	switch h.Get(contentEnconding) {
	case "gzip":
		reader, err = gzip.NewReader(b)
		if err != nil {
			return nil, err
		}
		defer reader.Close()
	default:
		reader = b
	}

	return reader, nil
}
