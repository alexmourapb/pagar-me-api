package get_card

import (
	"fmt"
	"io"
	"net/http"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/test"
)

func TestGetBody(t *testing.T) {

	var body io.ReadCloser
	var resp http.Response

	_, err := GetBody(body, resp.Header)
	test.AssertOk(t, err)
}

func Test_Get_Card_Structs(t *testing.T) {

	type Fields struct {
		fieldName string
		fieldType string
		tags      string
	}

	var tt = []struct {
		modelName   string
		modelStruct interface{}
		fields      []Fields
	}{
		//Test getCardNumberRequest struct
		{"getCardNumberRequest", &getCardNumberRequest{}, []Fields{
			{"CustomerID", "string", `json:"customerID"`}}},

		//Test getCardNumberResponse struct
		{"LambdaGetCardNumberResponse", &getCardNumberResponse{}, []Fields{
			{"CardNumber", "string", `json:"cardNumber"`},
			{"CVV", "string", `json:"cvv"`}}},
	}
	for _, tc := range tt {

		reflectionElements := reflect.ValueOf(tc.modelStruct).Elem()
		for i, field := range tc.fields {

			testName := fmt.Sprintf("%s %s", tc.modelName, field.fieldName)
			t.Run(testName, func(t *testing.T) {

				// Check field name
				v := reflectionElements.FieldByName(field.fieldName)
				assert.True(t, v.IsValid(), "Field '%s' not found in the struct!", field.fieldName)

				// Check field type
				ts := fmt.Sprintf("%v", v.Type())
				assert.Equal(t, ts, field.fieldType,
					"Field '%s' should be of type '%s', but received '%s'",
					field.fieldName, field.fieldType, ts)

				// Check field tag
				tag := fmt.Sprintf("%s", reflectionElements.Type().Field(i).Tag)
				assert.Equal(t, tag, field.tags,
					"Field '%s' tag changed. Tag expected: %s",
					field.fieldName, tag)
			})

		}

		assert.Equal(t, len(tc.fields), reflectionElements.NumField(),
			"Found %d elements in struct (%s) while waiting for %d",
			len(tt), tc.modelName, reflectionElements.NumField())
	}
}
