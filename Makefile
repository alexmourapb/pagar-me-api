dependency-install:
	docker pull postgres

dependency-start:
	docker run --name pagarme-postgres -p 5432:5432 -d postgres

dependency-db:
	docker exec -it pagarme-postgres psql -U postgres -c "CREATE DATABASE pagarme ENCODING 'LATIN1' TEMPLATE template0 LC_COLLATE 'C' LC_CTYPE 'C';"
	docker exec -it pagarme-postgres psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE pagarme TO postgres;"

dependency-test:
	docker exec -it pagarme-postgres psql -U postgres -c "CREATE DATABASE testdb ENCODING 'LATIN1' TEMPLATE template0 LC_COLLATE 'C' LC_CTYPE 'C';"
	docker exec -it pagarme-postgres psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE testdb TO postgres;"

run-db-test-fixtures-ci:
	psql -U postgres -h localhost -p 5432 -c 'create database testdb;'

build-linux:
	env GOOS=linux GOARCH=amd64 go build -o pagar-me-api ./cmd/api/main.go

build:
	go build -o pagar-me-api ./cmd/api/main.go

run-api:
	go run cmd/api/main.go -port=8181 -postgresURL=postgres://postgres:@localhost:5432/pagarme?sslmode=disable

test-all:
	go test ./... -v