package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	apiHandlers "bitbucket.org/alexmourapb/pagar-me-api/cmd/api/handlers"
	apiRouters "bitbucket.org/alexmourapb/pagar-me-api/cmd/api/routers"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/authorization"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/customer"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/infrastructure/environment"
	apiLogger "bitbucket.org/alexmourapb/pagar-me-api/internal/helper/infrastructure/logger"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/issuer"
)

const (
	defaultAppName = "Pagar-me-Api"
	defaultPort    = "8181"
)

var (
	port               string
	appName            string
	hsmURL             string
	postgresURL        string
	pagarmeEnvironment environment.Environment
)

func init() {
	checkEnvironment(os.Getenv("PAGARME_ENVIRONMENT"))
	flag.StringVar(&port, "port", os.Getenv("PORT"), "-port=8181")
	flag.StringVar(&appName, "appName", os.Getenv("APP_NAME"), "-appName=NameApp")
	flag.StringVar(&hsmURL, "hsmURL", os.Getenv("HSM_URL"), "-hsmURL=HsmURL")
	flag.StringVar(&postgresURL, "postgresURL", os.Getenv("DATABASE_URL"), "-postgresURL=postgres://postgres:@localhost:5432/production?sslmode=disable")
}

//Checks the environment in which it is running
func checkEnvironment(env string) {

	// Compare and validate environments
	if !environment.IsValid(env) {
		panic("PAGARME_ENVIRONMENT not setted!\n" +
			"To correct this, run this command in your terminal: export PAGARME_ENVIRONMENT=development\n" +
			"Available environments: development, staging, production, test")
	}

	pagarmeEnvironment = environment.FromString(env)

	// Detect if environment is under test-mode.
	if strings.HasSuffix(os.Args[0], ".test") || strings.Contains(os.Args[0], "/_test/") && env != environment.Test {

		// Production and Sandbox environment DO NOT support tests!
		if env == environment.Production || env == environment.Sandbox {
			panic(env + " environment DO NOT support tests!")
		}

		// Change environment automatically to "test" if necessary.
		if env != environment.Test {
			fmt.Printf("PAGARME Environment is setted to '%s', but is running under 'test' mode.\n", pagarmeEnvironment)
			fmt.Println("PAGARME Environment changed to 'test'")
			pagarmeEnvironment = environment.Test
		}
	}
}

func main() {
	flag.Parse()

	// Logger
	logger := apiLogger.Config(appName)
	logger.Info().Msg("Logger successfully initialized")

	logger.Info().Msg("Initializing APP_NAME")
	if appName == "" {
		_ = os.Setenv("APP_NAME", defaultAppName)
		appName = defaultAppName
		logger.Error().Msg("APP_NAME is empty, setting default: " + defaultAppName)
	}

	logger.Info().Msg("Initializing Port Number")
	if port == "" {
		_ = os.Setenv("PORT", defaultPort)
		port = defaultPort
		logger.Error().Msg("PORT is empty, setting default: " + defaultPort)
	}

	logger.Info().Msg("Initializing HSM_URL")
	if hsmURL == "" {
		logger.Fatal().Msg("Error HSM_URL: can not be empty")
		panic("Error HSM_URL: can not be empty")
	}

	logger.Info().Msg("Initializing Postgres")
	db, err := gorm.Open("postgres", postgresURL)
	if err != nil {
		logger.Fatal().Msg("can't initialize Postgres " + err.Error())
		panic("Error DB: " + err.Error())
	}
	defer db.Close()

	logger.Info().Msg("Initializing AutoMigrate Customers into Postgres")
	if err := db.AutoMigrate(&customer.Customer{}).Error; err != nil {
		logger.Error().Msg("can't AutoMigrate Customers into Postgres " + err.Error())
	}

	logger.Info().Msg("Initializing AutoMigrate CustomerAccounts into Postgres")
	if err := db.AutoMigrate(&customer.CustomerAccount{}).Error; err != nil {
		logger.Error().Msg("can't AutoMigrate CustomerAccounts into Postgres " + err.Error())
	}

	logger.Info().Msg("Initializing AutoMigrate Issuers into Postgres")
	if err := db.AutoMigrate(&issuer.Issuer{}).Error; err != nil {
		logger.Error().Msg("can't AutoMigrate Issuers into Postgres " + err.Error())
	}

	logger.Info().Msg("Initializing AutoMigrate Authorizations into Postgres")
	if err := db.AutoMigrate(&authorization.Authorization{}).Error; err != nil {
		logger.Error().Msg("can't AutoMigrate Authorizations into Postgres " + err.Error())
	}

	logger.Info().Msg("Initializing AutoMigrate AuthorizationTransactionItems into Postgres")
	if err := db.AutoMigrate(&authorization.AuthorizationTransactionItem{}).Error; err != nil {
		logger.Error().Msg("can't AutoMigrate AuthorizationTransactionItems into Postgres " + err.Error())
	}

	h := &apiHandlers.Handler{Environment: pagarmeEnvironment, DB: db, Logger: &logger}
	routes := apiRouters.Router(h)

	s := &http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      routes,
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 10,
	}

	logger.Info().Msg(appName + " listening on port: " + port)
	if err := s.ListenAndServe(); err != nil {
		log.Println("can't initialize Server ->", err)
		os.Exit(1)
	}
}
