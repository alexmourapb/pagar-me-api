package handlers

import (
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/render"
)

func ValidateRequest(r *http.Request, value render.Binder) error {
	if err := render.Bind(r, value); err != nil {
		return err
	}
	return nil
}

func Validate(data interface{}) error {

	_, err := govalidator.ValidateStruct(data)
	return err
}
