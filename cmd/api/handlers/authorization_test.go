package handlers_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	apiHandlers "bitbucket.org/alexmourapb/pagar-me-api/cmd/api/handlers"
	apiRouters "bitbucket.org/alexmourapb/pagar-me-api/cmd/api/routers"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/authorization"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/customer"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/infrastructure/environment"
	apiLogger "bitbucket.org/alexmourapb/pagar-me-api/internal/helper/infrastructure/logger"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/middleware"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/test"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/issuer"
)

func Test_AuthorizationHandlerWithSuccess(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusOK, w.Code)

	response := new(apiHandlers.AuthorizationResponse)
	decoder := json.NewDecoder(w.Body)
	err = decoder.Decode(&response)
	if err != nil {
		t.Fatal(err.Error())
	}
	test.AssertEquals(t, response.AcquirerStatus, apiHandlers.StatusApproved)
}

func Test_AuthorizationHandlerWithIssuerError(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err := json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}

func Test_AuthorizationHandlerIssuerWithInvalidStatus(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "blocked",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}

func Test_AuthorizationHandlerWithCustomerError(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}

func Test_AuthorizationHandlerCustomerWithStatusIInvalid(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "blocked",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}

func Test_AuthorizationHandlerAccountError(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	db.Create(issuerData)
	db.Create(customerData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}

func Test_AuthorizationHandlerAccountWithStatusIInvalid(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "blocked",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}

func Test_AuthorizationHandlerWithTotalAmountNotEqual(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)
	amountItem := decimal.NewFromFloat(1.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amountItem,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}

func Test_AuthorizationHandlerWithCustomerIDNotEqual(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     uuid.NewV4(),
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}

func Test_AuthorizationHandlerWithInsertAuthorizationError(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusInternalServerError, w.Code)
}

func Test_AuthorizationHandlerWithInsertTransactionItemsError(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusInternalServerError, w.Code)
}

func Test_AuthorizationHandlerWithInvalidRequest(t *testing.T) {

	logger := apiLogger.Config("pagar-me-test")

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(issuer.Issuer{}.TableName())
	defer db.DropTable(customer.Customer{}.TableName())
	defer db.DropTable(customer.CustomerAccount{}.TableName())
	defer db.DropTable(authorization.Authorization{}.TableName())
	defer db.DropTable(authorization.AuthorizationTransactionItem{}.TableName())

	db.AutoMigrate(issuer.Issuer{})
	db.AutoMigrate(customer.Customer{})
	db.AutoMigrate(customer.CustomerAccount{})
	db.AutoMigrate(authorization.Authorization{})
	db.AutoMigrate(authorization.AuthorizationTransactionItem{})

	issuerID, err := uuid.FromString("7c811a28-81a2-4ddb-8a15-67d7771509bb")
	test.AssertOk(t, err)

	issuerData := &issuer.Issuer{
		ID:     issuerID,
		Name:   "issuer test name",
		CNPJ:   "02559765000191",
		Status: "active",
	}

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customer.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customer.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		ExternalID:     "87f8e2ba-55cc-4d0a-a68c-04c54ef41944",
		ExpirationDate: "10/2021",
		CardBrand:      "visa",
		Status:         "active",
	}

	db.Create(issuerData)
	db.Create(customerData)
	db.Create(customerAccountData)

	var transactionItems []apiHandlers.AuthorizationTransactionsItemsRequest
	transactionItem := apiHandlers.AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	request := apiHandlers.AuthorizationRequest{
		CPF:                  "96558",
		AccountID:            customerAccountID.String(),
		TransactionID:        uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
		TransactionItems:     transactionItems,
	}

	payload := new(bytes.Buffer)
	err = json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/authorization", payload)
	test.AssertOk(t, err)

	clientID := os.Getenv("CLIENTID_TEST")
	clientSecret := os.Getenv("CLIENTSECRET_TEST")

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add(middleware.ClientIDHeaderName, clientID)
	r.Header.Add(middleware.ClientSecretHeaderName, clientSecret)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Environment: environment.Test}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)
}
