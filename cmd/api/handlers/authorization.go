package handlers

import (
	"net/http"
	"sync"

	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/authorization"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/customer"
	helperHandlers "bitbucket.org/alexmourapb/pagar-me-api/internal/helper/handlers"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/middleware"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/io/acquirer"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/io/get_card"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/issuer"
)

func (h *Handler) AuthorizationHandler(w http.ResponseWriter, r *http.Request) {

	//Validate request
	request := &AuthorizationRequest{}
	err := ValidateRequest(r, request)
	if err != nil {
		h.Logger.Error().Msg(err.Error())
		helperHandlers.Respond(w, r, http.StatusBadRequest, err.Error())
		return
	}

	//Get the issuerID from the context
	issuerID, ok := r.Context().Value(middleware.IssuerIDContextName).(string)
	if !ok {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "failed to retrieve issuer-id")
		return
	}

	var wg sync.WaitGroup
	issuerData := &issuer.Issuer{}
	customerData := &customer.Customer{}
	accountData := &customer.CustomerAccount{}
	var errIssuer, errCustomer, errAccount error

	wg.Add(3)

	go func() {
		defer wg.Done()
		//Retrieve Issuer Data
		issuerData, errIssuer = issuer.GetIssuerByID(h.DB, issuerID)
	}()

	go func() {
		defer wg.Done()
		//Retrieve Customer Data
		customerData, errCustomer = customer.GetCustomerByCPF(h.DB, request.CPF)
	}()

	go func() {
		defer wg.Done()
		//Retrieve Customer Account Data
		accountData, errAccount = customer.GetCustomerAccountByID(h.DB, request.AccountID)
	}()

	wg.Wait()

	if errIssuer != nil {
		h.Logger.Error().Msg(errIssuer.Error())
		helperHandlers.Respond(w, r, http.StatusBadRequest, errIssuer.Error())
		return
	}

	if errAccount != nil {
		h.Logger.Error().Msg(errAccount.Error())
		helperHandlers.Respond(w, r, http.StatusBadRequest, errAccount.Error())
		return
	}

	if errCustomer != nil {
		h.Logger.Error().Msg(errCustomer.Error())
		helperHandlers.Respond(w, r, http.StatusBadRequest, errCustomer.Error())
		return
	}

	if customerData.Status != StatusActive {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "customer status not active")
		return
	}

	if accountData.Status != StatusActive {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "account status not active")
		return
	}

	if issuerData.Status != StatusActive {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "issuer status not active")
		return
	}

	if customerData.ID != accountData.CustomerID {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "account not valid")
		return
	}

	transactionIDUUID, _ := uuid.FromString(request.TransactionID)

	authorizationInput := &authorization.Authorization{
		TransactionID:        transactionIDUUID,
		IssuerID:             issuerData.ID,
		Amount:               request.Amount,
		CustomerAccountID:    accountData.ID,
		NumberOfInstallments: request.NumberOfInstallments,
	}

	//Persist authorization data with "pending status"
	authorizationOutput, err := authorization.InsertAuthorization(h.DB, authorizationInput)
	if err != nil {
		h.Logger.Error().Msg(err.Error())
		helperHandlers.Respond(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	var sumItems decimal.Decimal
	transactionItems := []authorization.AuthorizationTransactionItem{}
	transactionItem := &authorization.AuthorizationTransactionItem{}
	for _, item := range request.TransactionItems {
		transactionItem.CodItem = item.CodItem
		transactionItem.Amount = item.Amount
		transactionItem.Descriprion = item.Descriprion
		sumItems = decimal.Sum(sumItems, item.Amount)
		//Persistent transaction items data
		itemOutput, err := authorization.InsertAuthorizationTransactionItem(h.DB, transactionItem)
		if err != nil {
			h.Logger.Error().Msg(err.Error())
			helperHandlers.Respond(w, r, http.StatusInternalServerError, err.Error())
			return
		}
		transactionItems = append(transactionItems, *itemOutput)
	}

	//Checks whether the total amount equals the total amount of the items.
	if sumItems.String() != request.Amount.String() {
		h.Logger.Error().Msg("total value differs from total value of items")
		helperHandlers.Respond(w, r, http.StatusBadRequest, "total value differs from total value of items")
		return
	}

	//Get sensitive customer card data from Lambda service simulating HSM
	card, err := get_card.GetCardData(r, accountData.ExternalID)
	if err != nil {
		h.Logger.Error().Msg(err.Error())
		helperHandlers.Respond(w, r, http.StatusBadRequest, err.Error())
		return
	}

	acquirerRequest := &acquirer.AcquirerAuthorizationRequest{
		AccountNumber:        card.CardNumber,
		CardCvv2Value:        card.CVV,
		CardExpiryDate:       accountData.ExpirationDate,
		OrderID:              request.TransactionID,
		Name:                 customerData.Name,
		Amount:               request.Amount.String(),
		Currency:             request.Currency,
		NumberOfInstallments: string(request.NumberOfInstallments),
	}

	//Submit data for authorization on Lambda service that simulates the acquirer
	acquirerResponse, err := acquirer.GetAuthorization(accountData.CardBrand, acquirerRequest)
	if err != nil {
		h.Logger.Error().Msg(err.Error())
		helperHandlers.Respond(w, r, http.StatusBadRequest, err.Error())
		return
	}

	//Persist authorization data and change status to processed in DB
	finish, err := authorization.FinishAuthorization(h.DB, authorizationOutput.ID.String(), acquirerResponse.Status, acquirerResponse.AuthID, acquirerResponse.Message)
	if err != nil {
		h.Logger.Fatal().Msg(err.Error())
		helperHandlers.Respond(w, r, http.StatusBadRequest, err.Error())
		return
	}

	response := &AuthorizationResponse{
		ID:                   finish.ID.String(),
		TransactionID:        finish.TransactionID.String(),
		AccountID:            finish.CustomerAccountID.String(),
		Name:                 customerData.Name,
		CPF:                  request.CPF,
		Currency:             finish.Currency,
		Amount:               finish.Amount,
		NumberOfInstallments: finish.NumberOfInstallments,
		IssuerName:           issuerData.Name,
		Status:               finish.Status,
		UpdatedAt:            finish.UpdatedAt,
		AcquirerAuthID:       finish.AcquirerAuthID,
		AcquirerStatus:       finish.AcquirerStatusResponse,
		AcquirerMessage:      finish.AcquirerMessageResponse,
	}

	helperHandlers.Respond(w, r, http.StatusOK, response)
}
