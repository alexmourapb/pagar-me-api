package handlers

import (
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog"

	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/infrastructure/environment"
)

type Handler struct {
	Environment environment.Environment
	DB          *gorm.DB
	Logger      *zerolog.Logger
}
