package handlers

import (
	"errors"
	"net/http"
	"time"

	"github.com/Nhanderu/brdoc"
	"github.com/asaskevich/govalidator"
	"github.com/shopspring/decimal"
)

const (
	StatusActive   = "active"
	StatusApproved = "approved"
)

type AuthorizationRequest struct {
	CPF string `json:"cpf" valid:"required"`

	TransactionID string `json:"transaction-id" valid:"required"`
	AccountID     string `json:"account-id" valid:"required"`

	Amount               decimal.Decimal `json:"amount" valid:"required"`
	Currency             string          `json:"currency,omitempty"`
	NumberOfInstallments uint            `json:"number-of-installments" valid:"required"`

	TransactionItems []AuthorizationTransactionsItemsRequest `json:"transaction-items" valid:"required"`
}

type AuthorizationTransactionsItemsRequest struct {
	CodItem     string          `json:"cod-item" valid:"required"`
	Descriprion string          `json:"description" valid:"required"`
	Amount      decimal.Decimal `json:"amount" valid:"required"`
}

func (c *AuthorizationRequest) Bind(r *http.Request) error {

	_, err := govalidator.ValidateStruct(*c)
	if err != nil {
		return err
	}

	if !brdoc.IsCPF(c.CPF) {
		return errors.New("invalid cpf")
	}

	if !govalidator.IsUUIDv4(c.AccountID) {
		return errors.New("account-id is not a valid uuid")
	}

	if !govalidator.IsUUIDv4(c.TransactionID) {
		return errors.New("transaction-id is not a valid uuid")
	}

	if c.Amount.LessThanOrEqual(decimal.Zero) {
		return errors.New("amount must be greater than zero")
	}

	if c.Currency == "" {
		c.Currency = "BRL"
	}

	return err
}

type AuthorizationResponse struct {
	ID   string `json:"authorization-id"`
	Name string `json:"name"`
	CPF  string `json:"cpf"`

	TransactionID        string          `json:"transaction-id"`
	AccountID            string          `json:"account-id"`
	Amount               decimal.Decimal `json:"amount"`
	Currency             string          `json:"currency"`
	NumberOfInstallments uint            `json:"number-of-installments"`
	IssuerName           string          `json:"issuer-name"`

	Status    string    `json:"status"`
	UpdatedAt time.Time `json:"updated-at"`

	AcquirerAuthID  string `json:"acquirer-auth-id"`
	AcquirerStatus  string `json:"acquirer-status"`
	AcquirerMessage string `json:"acquirer-message"`
}
