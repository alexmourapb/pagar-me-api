package handlers

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
)

func Test_AuthorizationRequest_Bind(t *testing.T) {

	amount := decimal.NewFromFloat(10.0)
	amountZero := decimal.NewFromFloat(0)
	var transactionItems []AuthorizationTransactionsItemsRequest
	transactionItem := AuthorizationTransactionsItemsRequest{
		CodItem:     "cod01",
		Descriprion: "item 01",
		Amount:      amount,
	}
	transactionItems = append(transactionItems, transactionItem)

	var tt = []struct {
		isValid bool
		request AuthorizationRequest
		message string
	}{
		{
			isValid: false,
			request: AuthorizationRequest{},
			message: "Empty request is not valid!",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				Amount:               amount,
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "Empty cpf request is not valid!",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:                  "72535313099",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				Amount:               amount,
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "invalid cpf",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:                  "72535313022",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				Amount:               amount,
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "Empty account-id request is not valid!",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b59277",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				Amount:               amount,
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "account-id is not a valid uuid",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				Amount:               amount,
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "Empty transaction-id request is not valid!",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6",
				Amount:               amount,
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "transaction-id is not a valid uuid",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "Empty amount request is not valid!",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				Amount:               amountZero,
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "amount must be greater than zero",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:              "72535313022",
				AccountID:        "57e65d60-58bf-406c-864c-f305b5927785",
				TransactionID:    "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				Amount:           amount,
				TransactionItems: transactionItems,
			},
			message: "Empty number-of-installments request is not valid!",
		},
		{
			isValid: false,
			request: AuthorizationRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				Amount:               amount,
				NumberOfInstallments: 2,
			},
			message: "Empty transaction-items request is not valid!",
		},
		{
			isValid: true,
			request: AuthorizationRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				TransactionID:        "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
				Amount:               amount,
				NumberOfInstallments: 2,
				TransactionItems:     transactionItems,
			},
			message: "",
		},
	}
	for _, tc := range tt {
		testName := tc.message
		t.Run(testName, func(t *testing.T) {

			err := tc.request.Bind(nil)
			if tc.isValid == true {
				assert.Nil(t, err)
			} else {
				assert.NotNil(t, err, tc.message)
			}
		})
	}
}

func Test_Handlers_Structs(t *testing.T) {

	type Fields struct {
		fieldName string
		fieldType string
		tags      string
	}

	var tt = []struct {
		modelName   string
		modelStruct interface{}
		fields      []Fields
	}{
		//Test AuthorizationRequest struct
		{"AuthorizationRequest", &AuthorizationRequest{}, []Fields{
			{"CPF", "string", `json:"cpf" valid:"required"`},
			{"TransactionID", "string", `json:"transaction-id" valid:"required"`},
			{"AccountID", "string", `json:"account-id" valid:"required"`},
			{"Amount", "decimal.Decimal", `json:"amount" valid:"required"`},
			{"Currency", "string", `json:"currency,omitempty"`},
			{"NumberOfInstallments", "uint", `json:"number-of-installments" valid:"required"`},
			{"TransactionItems", "[]handlers.AuthorizationTransactionsItemsRequest", `json:"transaction-items" valid:"required"`}}},

		//Test AuthorizationTransactionsItemsRequest struct
		{"AuthorizationTransactionsItemsRequest", &AuthorizationTransactionsItemsRequest{}, []Fields{
			{"CodItem", "string", `json:"cod-item" valid:"required"`},
			{"Descriprion", "string", `json:"description" valid:"required"`},
			{"Amount", "decimal.Decimal", `json:"amount" valid:"required"`}}},

		//Test AuthorizationResponse struct
		{"AuthorizationResponse", &AuthorizationResponse{}, []Fields{
			{"ID", "string", `json:"authorization-id"`},
			{"Name", "string", `json:"name"`},
			{"CPF", "string", `json:"cpf"`},
			{"TransactionID", "string", `json:"transaction-id"`},
			{"AccountID", "string", `json:"account-id"`},
			{"Amount", "decimal.Decimal", `json:"amount"`},
			{"Currency", "string", `json:"currency"`},
			{"NumberOfInstallments", "uint", `json:"number-of-installments"`},
			{"IssuerName", "string", `json:"issuer-name"`},
			{"Status", "string", `json:"status"`},
			{"UpdatedAt", "time.Time", `json:"updated-at"`},
			{"AcquirerAuthID", "string", `json:"acquirer-auth-id"`},
			{"AcquirerStatus", "string", `json:"acquirer-status"`},
			{"AcquirerMessage", "string", `json:"acquirer-message"`}}},
	}
	for _, tc := range tt {

		reflectionElements := reflect.ValueOf(tc.modelStruct).Elem()
		for i, field := range tc.fields {

			testName := fmt.Sprintf("%s %s", tc.modelName, field.fieldName)
			t.Run(testName, func(t *testing.T) {

				// Check field name
				v := reflectionElements.FieldByName(field.fieldName)
				assert.True(t, v.IsValid(), "Field '%s' not found in the struct!", field.fieldName)

				// Check field type
				ts := fmt.Sprintf("%v", v.Type())
				assert.Equal(t, ts, field.fieldType,
					"Field '%s' should be of type '%s', but received '%s'",
					field.fieldName, field.fieldType, ts)

				// Check field tag
				tag := fmt.Sprintf("%s", reflectionElements.Type().Field(i).Tag)
				assert.Equal(t, tag, field.tags,
					"Field '%s' tag changed. Tag expected: %s",
					field.fieldName, tag)
			})

		}

		assert.Equal(t, len(tc.fields), reflectionElements.NumField(),
			"Found %d elements in struct (%s) while waiting for %d",
			len(tt), tc.modelName, reflectionElements.NumField())
	}
}
