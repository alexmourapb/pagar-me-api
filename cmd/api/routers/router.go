package routers

import (
	"net/http"

	"github.com/go-chi/chi"

	apiHandlers "bitbucket.org/alexmourapb/pagar-me-api/cmd/api/handlers"
	helperHandlers "bitbucket.org/alexmourapb/pagar-me-api/internal/helper/handlers"
	"bitbucket.org/alexmourapb/pagar-me-api/internal/helper/middleware"
)

//Router define the routes to the resources and methods
func Router(h *apiHandlers.Handler) *chi.Mux {

	router := chi.NewRouter()

	h.Logger.Info().Msg("Setup routes")

	//Check if the service is available.
	router.Get("/health", helperHandlers.HandleHealthCheck)

	router.Mount("/authorization", authorizationRouter(h))

	return router
}

func authorizationRouter(h *apiHandlers.Handler) http.Handler {

	subRouter := chi.NewRouter()

	//Authentication middleware
	subRouter.Use(middleware.SecretMiddleware())

	//Authorization route
	subRouter.Post("/", func(w http.ResponseWriter, r *http.Request) {
		middleware.Recover(middleware.RouteMonitoring("POST", "/authorization", h.Logger, h.AuthorizationHandler)).ServeHTTP(w, r)
	})

	return subRouter
}
