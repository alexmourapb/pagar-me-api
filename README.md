# pagar-me-api

You must already have the docker installed, ok?

Requirements
------------------
```
Environment variables required:
PAGARME_ENVIRONMENT=sandbox
DATABASE_URL=postgres://postgres:@localhost:5432/pagarme?sslmode\=disable
PAGARME_AWS_ACCESS= sent-by-email
PAGARME_AWS_SECRET=sent-by-email
PAGARME_AWS_SECRET_MANAGER_NAME=sent-by-email
HSM_URL=https://14gdgpmwrh.execute-api.us-east-1.amazonaws.com/hmg/getcard
ACQUIRER_CLIENTID=sent-by-email
ACQUIRER_SECRET=sent-by-email
CLIENTID_TEST=sent-by-email
CLIENTSECRET_TEST=sent-by-email
```

Running the application first time
------------------
```
make dependency-install
make dependency-start
make dependency-db
make dependency-test
make run-api
```

Running the test first time
------------------
```
make dependency-install
make dependency-start
make dependency-test
make test-all
```

Consuming the API
------------------

Request:
```
curl -X POST \
  http://187.19.204.244:8181/authorization \
  -H 'Client-Id: pagar-me-client-id-test' \
  -H 'Client-Secret: client-secret-sent-by-email \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
	"cpf":"72535313022",
	"account-id":"57e65d60-58bf-406c-864c-f305b5927785",
	"transaction-id":"96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
	"amount": 10.0,
	"number-of-installments": 4,
	"transaction-items": 
	[
		{
			"cod-item": "cod01",
			"description": "item01 teste",
			"amount": 10.0
    }		
	]
}'
```

Response:
```
{
    "authorization-id": "1749c7e3-a698-4c6c-a074-3b06c73e60b2",
    "name": "Cliente Teste 04",
    "cpf": "72535313022",
    "transaction-id": "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
    "account-id": "57e65d60-58bf-406c-864c-f305b5927785",
    "amount": "10",
    "currency": "BRL",
    "number-of-installments": 4,
    "issuer-name": "Emissor de teste 01",
    "status": "processed",
    "updated-at": "2019-12-30T22:15:21.625308001-03:00",
    "acquirer-auth-id": "631378b0-4bc4-41a5-8538-39792a9dda61",
    "acquirer-status": "approved",
    "acquirer-message": "approved ok"
}
```

Valid Payloads
------------------

Request 01:
```
{
	"cpf":"96558156415",
	"account-id":"5d544dee-3327-4f1a-8970-64e8928137a2",
	"transaction-id":"3ea24f2f-6920-416b-ae53-d38e10ebdfb1",
	"amount": 20.10,
	"number-of-installments": 2,
	"transaction-items": 
	[
		{
			"cod-item": "cod01",
			"description": "item01 teste",
			"amount": 10
		},
		{
			"cod-item": "cod02",
			"description": "item02 teste",
			"amount": 10.1
		}
		
	]
}
```

Request 02:
```
{
	"cpf":"46500301889",
	"account-id":"1bf3826e-f9ca-4d1d-b4cf-208aa0afbe06",
	"transaction-id":"3f931a91-d7d9-4970-be0f-8d033e026636",
	"amount": 90.0,
	"number-of-installments": 4,
	"transaction-items": 
	[
		{
			"cod-item": "cod01",
			"description": "item01 teste",
			"amount": 10.0
    },
		{
			"cod-item": "cod02",
			"description": "item02 teste",
			"amount": 30.0
		},
    {
			"cod-item": "cod03",
			"description": "item03 teste",
			"amount": 50.0
		}		
	]
}
```

Request 03:
```
{
	"cpf":"02792449403",
	"account-id":"ceb6b265-2957-47d8-96d5-5dda996d37ea",
	"transaction-id":"bccd5d05-a7b1-466a-a9d2-57d32561b27a",
	"amount": 30.90,
	"number-of-installments": 3,
	"transaction-items": 
	[
		{
			"cod-item": "cod01",
			"description": "item01 teste",
			"amount": 30.9
		}		
	]
}
```

Request 04:
```
{
	"cpf":"72535313022",
	"account-id":"57e65d60-58bf-406c-864c-f305b5927785",
	"transaction-id":"96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
	"amount": 100.0,
	"number-of-installments": 4,
	"transaction-items": 
	[
		{
			"cod-item": "cod01",
			"description": "item01 teste",
			"amount": 100.0
    }		
	]
}
```