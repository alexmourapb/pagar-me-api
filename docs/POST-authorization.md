## **Resumo: Solicitação de autorização de compra com cartão de crédito**
**Descrição:** Este recurso é responsável por persistir os dados de uma compra com 
cartão de crédito e solicitar a autorização junto a adquirente. 

**Nome da Tag:** Authorization

**Path:** POST /authorization

**Nome do Método:** AuthorizationHandler

## **Requisição**

| Parâmetro | Descrição | Tipo parâmetro | Tipo de dados | Exemplo | Validador |
|-|-|-|-|-|-|
| NOME_PARAMETRO            | DESCRICAO_PARAMETRO | TIPO_PARAMETRO | TIPO_DADO (Long, BigDecimal, String) | EXEMPLO | VALIDADOR |
| cpf                       | CPF do cliente                 | Body    | string     | "Boleto teste"                          | Required |
| account-id                | Código da conta do cliente     | Body    | string     | "57e65d60-58bf-406c-864c-f305b5927785"  | Required |
| transaction-id            | Código da transação            | Body    | string     | "96bc75f2-5108-4b50-bc86-1223fc8bd6fd"  | Required |
| amount                    | Valor total da transação       | Body    | float      | "Cedente teste"                         | Required |
| currency                  | Código da moeda                | Body    | string     | "BRL"                                   | Optional |
| number-of-installments    | Número de parcelas             | Body    | integer    | 10.00                                   | Required |
| transaction-items         | Itens da transação             | Body    | array      |                                         | Required |
| cod-item                  | Código do item                 | Body    | string     | "Cod01"                                 | Required |
| description               | Descrição do item              | Body    | string     | "Descricao do item 01"                  | Required |
| amount                    | Valor do item                  | Body    | float      | 10.00                                   | Required |

### **Exemplo Request:**
```json
    {
    	"cpf":"72535313022",
    	"account-id":"57e65d60-58bf-406c-864c-f305b5927785",
    	"transaction-id":"96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
    	"amount": 10.0,
    	"number-of-installments": 4,
    	"transaction-items": 
    	[
    		{
    			"cod-item": "cod01",
    			"description": "item01 teste",
    			"amount": 10.0
        }		
    	]
    }
```

## **Resposta**

|Parâmetro | Descrição | Tipo de dados | Exemplo | 
|-|-|-|-|
| NOME_PARAMETRO | DESCRICAO_PARAMETRO | TIPO_DADO (Long, BigDecimal, String) | EXEMPLO |
| authorization-id          | Código autorização                      | string     | "54d41a5c-eb16-429c-aa71-2c8410bf9d75"                          
| name                      | Nome do cliente                         | string     | "Cliente Teste 04"                          
| cpf                       | CPF do cliente                          | string     | "72535313022"                          
| transaction-id            | Código da transação                     | string     | "96bc75f2-5108-4b50-bc86-1223fc8bd6fd"
| account-id                | Código da conta do cliente              | string     | "57e65d60-58bf-406c-864c-f305b5927785"    
| amount                    | Valor total da transação                | string     | "10"                         
| currency                  | Código da moeda                         | string     | "BRL"                                   
| number-of-installments    | Número de parcelas                      | integer    | 4                                   
| issuer-name               | Nome do emissor                         | string     | "Emissor de teste 01"                                        
| status                    | Status da autorização                   | string     | "processed"                                 
| updated-at                | Data e hora da autorização              | string     | "2019-12-30T20:57:57.316900399-03:00"                  
| acquirer-auth-id          | Código de autorização no adquirente     | string     | "50872d63-44d0-41b1-aff7-50fb8b9dae9f"                                   
| acquirer-status           | Status da autorização no adquirente     | string     | "approved"                                   
| acquirer-message          | Mensagem de autorização do adquirente   | string     | "approved ok"                                   

**Código status da resposta: 200**

### **Exemplo Response:**
```json
    {
      "authorization-id": "54d41a5c-eb16-429c-aa71-2c8410bf9d75",
      "name": "Cliente Teste 04",
      "cpf": "72535313022",
      "transaction-id": "96bc75f2-5108-4b50-bc86-1223fc8bd6fd",
      "account-id": "57e65d60-58bf-406c-864c-f305b5927785",
      "amount": "10",
      "currency": "BRL",
      "number-of-installments": 4,
      "issuer-name": "Emissor de teste 01",
      "status": "processed",
      "updated-at": "2019-12-30T20:57:57.316900399-03:00",
      "acquirer-auth-id": "50872d63-44d0-41b1-aff7-50fb8b9dae9f",
      "acquirer-status": "approved",
      "acquirer-message": "approved ok"
    }
```

## **Documentos e referência**

**GitHub Pagar.me :** https://github.com/pagarme/vagas/tree/3bfde038fbd5776280cf0eddde652cd5220fc035/desafios/software-engineer-golang

## **Detalhes de implementação**

**Pré-requisitos**
1. É preciso ter clientes, contas de clientes e emissor cadastrados;
2. É preciso client-id e client-secret válidos;

**Fluxo de execução**

1. Validar client-id e client-secret;
2. Validar parâmetros da requisição;
3. Persistir na base dados iniciais da autorização com status de "pending;
4. Pega os dados sensíveis do cartão do cliente no Lambda que emula o HSM;
5. Requisita em um dos Lambdas que emula o adquirente da conta selecionada a autorização da compra  
6. Persistir nna base os dados do retorno do adquirente e muda"o status para "processed;
7. Retornar com status code 200;

## **Casos de teste**

* Quando parâmetros nulos: 400 (bad request)
* Criar Autorização: 200 (Ok)